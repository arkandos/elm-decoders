import DecoderError from "./decoder-error";


export default class DecoderException extends Error {
    public constructor(
        public readonly errors: DecoderError[]
    ) {
        super(errorsToString(errors));
        this.name = 'DecoderException';
    }
}


function errorsToString(errors: DecoderError[]): string {
    const msg = 'Something went wrong while I tried to decode a value!\n' +
        errors.map(error => error.toString()).join('\n') +
        '\n';

    return msg;
}

