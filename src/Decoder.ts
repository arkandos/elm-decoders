import DecoderException from "./decoder-exception";
import DecoderError from "./decoder-error";

import Context from "./internal/context";
import Result from "./internal/result";

// Wrap optional values in a OptionalDecoder instead of a Decoder,
// but only if strictNullChecks is enabled
// (undefined is only assignable to number if it isn't)
type DD<T> = {
    1: Decoder<T>,
    0: undefined extends T ? OptionalDecoder<T> : Decoder<T>
}[undefined extends number ? 1 : 0];

type DecoderTable<T> = {
    [P in keyof T]-?: DD<T[P]>;
}


type DecoderTuple<Args extends any[]> = DecoderTable<Args>;


// A string => T indexed type
type Indexed<T> = {
    [key: string]: T;
}

// Extract the T from an Indexed<T>
type IndexedValue<T> =
    T extends Indexed<infer TValue> ? TValue : never;

// Extracts all explicit keys from a type.
// This is copied from https://stackoverflow.com/a/51955852/7329959,
// but additionally has a (semantically useless) `extends keyof T` test, to make sure that
// `forall T. KnownKeys<T> extends keyof T` afterwards.
type KnownKeys<T> = {
    [K in keyof T]: string extends K ? never : number extends K ? never : K
} extends { [_ in keyof T]: infer U } ? U extends keyof T ? U : never : never;

// returns a type only consiting of the explicit members, with index signatures removed.
type Explicit<T> = Pick<T, KnownKeys<T>>


/**
 * Turn (untyped) `any` values (for example coming from a configuration
 * file or some external API) into type-checked and type-safe Typescript values!
 *
 * This library is heavily inspired by [Elm's Json.Decode](https://package.elm-lang.org/packages/elm-lang/core/5.1.1/Json-Decode),
 * but adds some Typescript-specific features to it,
 * making it easier to use while also catching more mistakes at compile time.
 *
 * If you are comfortable with Elm/ML-like syntax, I highly recommend checking out
 * their [Intro to JSON decoders](https://guide.elm-lang.org/effects/json.html) for an
 * introduction behind the concepts of this library.
 *
 * This class itself represents a Decoder knowing how to turn an `any` into a `T`.
 */
export default class Decoder<T> {
    protected constructor(
        protected readonly _run: (ctx: Context) => Result<T>,
        protected readonly _isField: boolean = false
    ) { }

//#region "Public-Facing Decoding"

    /**
     * Parses the given string into a JSON value and run the decoder on it.
     * This function will fail if the input string is not valid JSON, or if
     * the Decoder fails for some reason.
     *
     * @returns The decoded object.
     * @throws {DecoderError} An exception containing a list of all the things that went wrong.
     */
    public decodeString(jsonString: string): T {
        try {
            const input: any = JSON.parse(jsonString);
            return this.decodeValue(input);
        } catch(e) {
            if (e instanceof SyntaxError) {
                throw new DecoderException([
                    DecoderError.jsonSyntaxError(e)
                ]);
            } else {
                throw e;
            }
        }
    }

    /**
     * Decodes a given value, returning a type-checked, fully constructed version of it.
     * This function will fail with an exception if the Decoder fails for some reason.
     *
     * @returns The decoded object.
     * @throws {DecoderError} An exception containing a list of all the things that went wrong.
     */
    public decodeValue(input: any): T {
        const ctx = Context.of(input);
        const result = this._run(ctx);
        return result.unwrap();
    }


    /**
     * Parse a given string into a JSON value and run the decoder on it.
     * Instead of throwing an exception, this method uses the functional
     * approach of returning a 'Either' instance to distinquish
     * success from failure.
     *
     * This function catches any JSON errors and packages them into a `Left`.
     * Any other exceptions will not be caught by using this method.
     *
     * @param Left 'Either' object constructor for the Failure case
     * @param Right 'Either' object constructor for the Success case
     * @param jsonString The string to parse and decode
     */
    public runString<U>(Left: (errors: DecoderError[]) => U, Right: (value: T) => U, jsonString: string): U {
        try {
            const input: any = JSON.parse(jsonString);
            return this.runValue(Left, Right, input);
        } catch (e) {
            if (e instanceof SyntaxError) {
                return Left([DecoderError.jsonSyntaxError(e)]);
            } else {
                throw e;
            }
        }
    }


    /**
     * Decode a value, putting the result into an Either type.
     * Instead of throwing an exception, this method uses the functional
     * approach of returning a 'Either' instance to distinquish
     * success from failure.
     *
     * This function will not catch exceptions for you, but allows you to
     * handle Decoder errors in a more functional way.
     *
     * Internally, this library uses a custom 'Result' Applicative type.
     *
     * @param Left 'Either' object constructor for the Failure case
     * @param Right 'Either' object constructor for the Success case
     * @param jsonString The string to parse and decode
     */
    public runValue<U>(Left: (errors: DecoderError[]) => U, Right: (value: T) => U, input: any): U {
        return this.fold(Left, Right).decodeValue(input);
    }

//#endregion

//#region "Primitives"

    /**
     * Do not decode the value, but return it as an untyped `any`.
     * This can be useful if you have a complex data structure you want to deal with later, or you just need to extract this value to pass it on, but you don't care about its structure.
     */
    public static readonly any: Decoder<any> = function () {
        return new Decoder<any>(function Decoder$any(ctx: Context): Result<any> {
            return Result.succeed(ctx.value);
        });
    }();


    /**
     * Decode a boolean. Only real `boolean` objects will be accepted,
     * so a string property `"true"` or a numeric value `1` will result in a failure.
     *
     * ```typescript
     * Decoder.boolean.decodeValue(true)   // true
     * Decoder.boolean.decodeValue(1)      // DecoderError
     * Decoder.boolean.decodeValue("true") // DecoderError
     * ```
     */
    public static readonly boolean: Decoder<boolean> = new Decoder<boolean>(function Decoder$boolean(ctx: Context): Result<boolean> {
        if (typeof ctx.value == 'boolean') {
            return Result.succeed(ctx.value);
        } else {
            return Result.fail(DecoderError.badPrimitive(ctx, "a boolean value (true, false)"));
        }
    });


    /**
     * Decode a number. Only real `number` objects will be accepted. Strings are generally not seen as numbers,
     * even if they only contain digits. The only exceptions to this rule are special string for
     * `"NaN"` and `"Infinity"`, respectively.
     *
     * ```typescript
     * Decoder.number.decodeValue(42)          // ==> 42
     * Decoder.number.decodeValue("42")        // ==> DecoderError
     * Decoder.number.decodeValue("NaN")       // ==> NaN
     * Decoder.number.decodeValue(Infinity)    // ==> Infinity
     * Decoder.number.decodeValue("-Infinity") // ==> -Infinity
     * ```
     */
    public static readonly number: Decoder<number> = new Decoder<number>(function Decoder$number(ctx: Context): Result<number> {
        const specialStringsMap: { [key: string]: number } = {
            'NaN': NaN,
            'Infinity': Infinity,
            '+Infinity': Infinity,
            '-Infinity': -Infinity
        };

        const input = ctx.value;

        if (typeof input == 'number') {
            return Result.succeed(input);
        } else if (typeof input == 'string' && specialStringsMap.hasOwnProperty(input)) {
            return Result.succeed(specialStringsMap[input]);
        } else {
            return Result.fail(DecoderError.badPrimitive(ctx, "a number"));
        }
    });


    /**
     * Decode a number that is an integer. In contrast to the `Decoder.number` decoder, this does not
     * accept `NaN` or `Infinite` values, and also rejects all numbers that cannot be encoded as a 32-bit integer
     * value, including all floating-point numbers.
     *
     * ```typescript
     * Decoder.int.decodeValue(42)              // ==> 42
     * Decoder.int.decodeValue(-0)              // ==> -0
     * Decoder.int.decodeValue(0.5)             // ==> DecoderError
     * Decoder.int.decodeValue(Math.pow(2, 32)) // ==> DecoderError
     * ```
     */
    public static readonly int: Decoder<number> = new Decoder<number>(function Decoder$int(ctx: Context): Result<number> {
        const input = ctx.value;
        if (typeof input == 'number') {
            if ((-2147483647 < input && input < 2147483647 && (input | 0) === input)
                || (isFinite(input) && !(input % 1))) {
                return Result.succeed(+input);
            }
        }
        return Result.fail(DecoderError.badPrimitive(ctx, "an integer"));
    });


    /**
     * Similar to `Decoder.number`, this decoder accepts all floating point numbers, but does not allow for special
     * values like `NaN` or `Infinite`, even if the object itself would be a valid number object.
     *
     * ```typescript
     * Decoder.float.decodeValue(42)          // ==> 42
     * Decoder.float.decodeValue("42")        // ==> DecoderError
     * Decoder.float.decodeValue("NaN")       // ==> DecoderError
     * Decoder.float.decodeValue(Infinity)    // ==> DecoderError
     * Decoder.float.decodeValue("-Infinity") // ==> DecoderError
     * ```
     */
    public static readonly float: Decoder<number> = new Decoder<number>(function Decoder$float(ctx: Context): Result<number> {
        if (typeof ctx.value == 'number' && isFinite(ctx.value)) {
            return Result.succeed(ctx.value);
        } else {
            return Result.fail(DecoderError.badPrimitive(ctx, "a valid floating-point number"));
        }
    });


    /**
     * Decode string values.
     * Only actual strings will be accepted by this decoder, so values like `null` or any other
     * value that could be `.toString()` - ed are going to be rejected.
     *
     * If you want to accept optional strings, consider using the [optional modifier]{@link Decoder.optional},
     * or create Decoder chains using {@link Decoder.oneOf}.
     *
     * ```typescript
     * Decoder.string.decodeValue("")                            // ==> ""
     * Decoder.string.decodeValue("Hello, Sailor!")              // ==> "Hello, Sailor!"
     * Decoder.string.decodeValue(null)                          // ==> DecoderError
     * Decoder.string.decodeValue({ toString() { return ""; } }) // ==> DecoderError
     * ```
     */
    public static readonly string: Decoder<string> = new Decoder<string>(function Decoder$string(ctx: Context): Result<string> {
        if (typeof ctx.value == 'string' || ctx.value instanceof String) {
            return Result.succeed(ctx.value + '');
        } else {
            return Result.fail(DecoderError.badPrimitive(ctx, "a string"));
        }
    });

//#endregion

//#region Composite Types

    /**
     * Create a typesafe decoder for an interface or a type alias.
     * The `configuration` object you pass in has the same *shape*
     * as the type to decode, but you have to specify decoders for
     * all the members instead. This ensures that your compile-time
     * type definition and your run-time decoder definition will always
     * stay in sync with one another.
     *
     * The shape of the `configuration` object is the same as the shape of your interface.
     * If your JSON has a slightly different shape, you can pass a {@link Decoder.field} or an
     * {@link Decoder.fieldOpt} as one of the parameters instead, ignoring the default mapping.
     *
     * If your JSON structure and your target interface structure are vastly different, you might
     * want to consider creating intermediate DTO interfaces instead, and using {@link Decoder.map}
     * to build your final objects.
     *
     * **Example:**
     *
     * ```typescript
     * interface Point {
     *     x: number;
     *     y: number;
     * }
     *
     * // ok: configuration has the same shape and the same types as Point
     * const pointDecoder = Decoder.obj<Point>({
     *     x: Decoder.float,
     *     y: Decoder.float
     * })
     *
     * // ok: configuration has the same shape as the Point interface, but x/y got renamed to u/v
     * const uvDecoder = Decoder.obj<Point>({
     *     x: Decoder.field('u', Decoder.float),
     *     y: Decoder.field('v', Decoder.float)
     * })
     *
     * // compile error: configuration is missing a field
     * const pointDecoder2 = Decoder.obj<Point>({
     *     x: Decoder.float
     * })
     *
     * // compile error: configuration and interface types don't match
     * const pointDecoder3 = Decoder.obj<Point>({
     *     x: Decoder.integer,
     *     y: Decoder.integer
     * })
     *
     * // compile error: configuration declares a non-optional field as optional
     * const pointDecoder4 = Decoder.obj<Point>({
     *     x: Decoder.integer,
     *     y: Decoder.integer.optional()
     * })
     * ```
     */
    public static obj<T>(configuration: DecoderTable<T>): Decoder<T> {
        const addField = (field: string) => (obj: any) => (value: any): any => {
            // :LocalMutability We are just adding fields here, so we do not copy the whole object,
            // on which we do not have a reference left.
            obj[field] = value;
            return obj;
        };

        // :UnsafeCast Typescript's Object.keys typing should be exactly this?!
        const fields: Array<Extract<keyof T, string>> = Object.keys(configuration) as any;

        // :UnsafeCast
        const wrappedConfiguration: DecoderTable<T> = fields.reduce((result: any, field) => {
            const valueDecoder: Decoder<T[Extract<keyof T, string>]> | OptionalDecoder<T[Extract<keyof T, string>]> = configuration[field];

            if (valueDecoder instanceof OptionalDecoder) {
                // :LocalMutability
                result[field] = valueDecoder._isField
                    ? valueDecoder
                    : Decoder.fieldOpt(field, valueDecoder);
            } else {
                // :LocalMutability
                result[field] = valueDecoder._isField
                    ? valueDecoder
                    : Decoder.field(field, valueDecoder);
            }

            return result;
        }, {})

        return new Decoder(function Decoder$obj(ctx: Context): Result<T> {
            if (typeof ctx.value != 'object' || ctx.value === null) {
                return Result.fail(DecoderError.badPrimitive(ctx, "a non-null object"));
            }

             // :UnsafeCast Theoretically this cast is safe at the end of the function, because we will have added all the properties back.
            return fields.reduce((result, field) => {
                const valueDecoder: Decoder<T[Extract<keyof T, string>]> | OptionalDecoder<T[Extract<keyof T, string>]> = wrappedConfiguration[field];

                // :TypeDependency
                const runDecoder = valueDecoder instanceof OptionalDecoder ? valueDecoder._run : valueDecoder._run;

                return result
                    .map(addField(field)) // :LocalMutability
                    .ap(runDecoder(ctx));
            }, Result.succeed<T>({} as T));
        });
    }


    /**
     * Create a decoder for types with an index signature.
     * Because index signatures can optionally have required keys, this function not only takes
     * a default `valueDecoder`, but also an `explicitConfiguration` map in the same format as
     * the one for {@link Decoder.obj}, allowing for explicit control over those fields.
     *
     * **Example:**
     *
     * ```typescript
     * type ComplexKey = { key: string, ctrl?: boolean, alt?: boolean, shift?: boolean };
     * type Key = string | ComplexKey;
     *
     * type Keymap = {
     *     [command: string]: Key,
     *     // editing keymap needs a keybinding, to prevent the user from locking themselfes out
     *     "settings.editKeymap": Key
     * };
     *
     * const keyDecoder =
     *     Decoder.string.orElse(
     *     Decoder.obj<ComplexKey>({
     *         key: Decoder.string,
     *         ctrl: Decoder.optional(Decoder.boolean),
     *         alt: Decoder.optional(Decoder.boolean),
     *         shift: Decoder.optional(Decoder.boolean)
     *     }));
     *
     * const keymapDecoder = Decoder.dict<Keymap>(keyDecoder, {
     *     "settings.editKeymap": keyDecoder
     * });
     * ```
     */
    public static dict<TDict extends Indexed<TValue>, TValue = IndexedValue<TDict>>(valueDecoder: Decoder<TValue>, explicitConfiguration: DecoderTable<Explicit<TDict>>): Decoder<TDict> {
        const addField = (field: string) => (obj: any) => (value: any): any => {
            // :LocalMutability We are just adding fields here, so we do not copy the whole object,
            // on which we do not have a reference left.
            obj[field] = value;
            return obj;
        };

        const explicitFields = Object.keys(explicitConfiguration);

        return Decoder.obj(explicitConfiguration).andThen(explicitValues => {
            return new Decoder<TDict>(function Decoder$dict(ctx: Context): Result<TDict> {
                if (typeof ctx.value != 'object' || ctx.value === null) {
                    return Result.fail(DecoderError.badPrimitive(ctx, "a non-null object"));
                }

                // only use fields that are not in the explicitConfiguration
                const fields = Object.keys(ctx.value).filter(field => explicitFields.indexOf(field) < 0);

                return fields.reduce((result, field) => {
                    return result
                        .map(addField(field)) // :LocalMutability
                        .ap(valueDecoder._run(ctx.goto(field)));
                }, Result.succeed(explicitValues));
            });
        });
    }


    /**
     * Decode a tuple of known parameter types.
     *
     * ```typescript
     * const keyValueDecoder = Decoder.tuple<[string, string]>(Decoder.string, Decoder.string);
     *
     * keyValueDecoder.decodeValue(["msg", "Hello, Sailor!"]); // OK
     * keyValueDecoder.decodeValue(["msg"]) // Err - second tuple argument missing
     * keyValueDecoder.decodeValue({0: "msg", 1: "Hello, Sailor!"}) // Err - not an array
     * ```
     *
     * While {@link Decoder.optional} works for allowing `null` - values inside the tuple, it will *NOT* accept
     * non-existent tuple elements. Similar to optional function paramters, this could lead to confusing results
     * once optional and required values are mixed.
     *
     * Instead, I recommend using {@link Decoder.oneOf} or {@link Decoder.andThen} to dispatch to multiple different
     * decoders for each number of values.
     */
    public static tuple<Args extends any[]>(...decoders: DecoderTuple<Args>): Decoder<Args> {
        const wrappedDecoders = decoders.map((decoder, index) =>
            decoder._isField ? decoder : Decoder.index(index, decoder));

        return new Decoder(function Decoder$tuple(ctx: Context): Result<Args> {
            return Result.sequence<any>(
                wrappedDecoders.map(decoder => decoder._run(ctx))
            ) as Result<Args>; // :UnsafeCast This cast is safe, but sequence does require all args to have a common type.
        });
    }


    /**
     * Convert a regular decoder into a decoder for arrays of that type.
     * Array decoders only succeed if all items can be successfully decoded.
     * If this is not your desired behaviour, you can use {@link Decoder.orDefaultValue} and {@link Decoder.map}
     * to return default values and (optionally) filter those out instead.
     *
     * **Example:**
     *
     * ```typescript
     * const d1 = Decoder.array(Decoder.int);
     *
     * d1.decodeValue([3, 4, 5])   // ==> [3, 4, 5]
     * d1.decodeValue([3, "4", 5]) // ==> DecoderError ...
     * d1.decodeValue([])          // ==> []
     *
     * d1.decodeValue({0: 3, 1: 4, 2: 5, length: 3}) // ==> DecoderError ...
     * ```
     */
    public static array<T>(itemDecoder: Decoder<T>): Decoder<T[]> {
        return new Decoder<T[]>(function Decoder$array(ctx: Context): Result<T[]> {
            const append = <T> (arr: T[]) => (value: T) => {
                // :LocalMutability We are just appending on the existing array here,
                // because we don't keep old references around!
                arr.push(value);
                return arr;
            };

            if (!(ctx.value instanceof Array)) {
                return Result.fail(DecoderError.badPrimitive(ctx, "an array"));
            }

            let result = Result.succeed<T[]>([]);

            const len = ctx.value.length;
            for (let i = 0; i < len; ++i) { // :LocalMutability
                // :LocalMutability
                result = result.map(append).ap(itemDecoder._run(ctx.goto(i)));
            }

            return result;
        });
    }


    /**
     * Create a decoder that extracts values to pass to a class constructor.
     *
     * **Example:**
     *
     * ```typescript
     * class Point {
     *     constructor(
     *         public readonly x: number,
     *         public readonly y: number
     *     ) { }
     * }
     *
     * const pointDecoder = Decoder.class(Point,
     *     Decoder.field('x', Decoder.int),
     *     Decoder.field('y', Decoder.int));
     * ```
     *
     * @param {new (...args: Args) => T} constructor  A class constructor to use to instanciate the resulting object.
     * @param {DecoderTuple<T>} argsDecoders A decoder for each parameter of the constructor.
     *
     */
    public static class<T, Args extends any[]>(constructor: new (...args: Args) => T, ... argsDecoders: DecoderTuple<Args>): Decoder<T> {
        // :CopyPaste
        // there is a bug in Typescript 3.1 that prevents us from just delegating to Decoder.function here:
        // spreading ...argsDecdoers infers their type to be OptionalDecoder<any>[] for some reason,
        // which is not compatible with DecoderTuple<Args>, of course.

        return new Decoder(function Decoder$function(ctx: Context): Result<T> {
            const argsResult: Result<Args> = Result.sequence<any>(
                argsDecoders.map(argDecoder => argDecoder._run(ctx))
            ) as Result<Args>; // :UnsafeCast This cast is safe, but sequence does require all args to have a common type.

            return argsResult.chain(args => {
                try {
                    return Result.succeed(new constructor(...args));
                } catch (e) {
                    return Result.fail(DecoderError.constructorException(ctx, constructor.name, e));
                }
            });
        });
    }


    /**
     * Create a decoder that extracts values to pass to a value-constructing function.
     * This is very similar to a static version of {@link Decoder.map} that supports multiple arguments.
     *
     * @param {(...args: Args) => T} constructor  A function to call to instanciate the resulting object.
     * @param {DecoderTuple<T>} argsDecoders A decoder for each parameter of the function.
     *
     */
    public static function<T, Args extends any[]>(constructor: (...args: Args) => T, ... argsDecoders: DecoderTuple<Args>): Decoder<T> {
        return new Decoder(function Decoder$function(ctx: Context): Result<T> {
            const argsResult: Result<Args> = Result.sequence<any>(
                argsDecoders.map(argDecoder => argDecoder._run(ctx))
            ) as Result<Args>; // :UnsafeCast This cast is safe, but sequence does require all args to have a common type.

            return argsResult.chain(args => {
                try {
                    return Result.succeed(constructor(... args));
                } catch(e) {
                    return Result.fail(DecoderError.constructorException(ctx, constructor.name, e));
                }
            });
        });
    }

//#endregion

//#region "Advanced data structures"

    /**
     * Creates a Decoder that checks for some hardcoded primitive value, as used in [literal types](https://basarat.gitbooks.io/typescript/docs/types/literal-types.html)
     * or discriminated unions. In contrast to the `succeed` decoder, this one does not
     * always return the same value, but only accepts excatly one value as the only valid one.
     */
    public static tag<T extends (boolean | number | string)>(value: T): Decoder<T> {
        return new Decoder<T>(function Decoder$hardcoded(ctx: Context): Result<T> {
            return ctx.value === value
                ? Result.succeed(value)
                : Result.fail(DecoderError.badPrimitive(ctx, DecoderError.getPreviewString(value)));
        });
    }

    /**
     * Makes a decoder that doesn't fail on `null` or `undefined`, but instead returns null, or a custom value.
     *
     * You may notice that this function returns an `OptionalDecoder<T>` instead of your regular
     * `Decoder<T>`. This is a hack used to enable typechecks on optional fields, both classes are
     * practially and semantically identical.
     *
     * Also note that this library works best with [strictNullChecks](https://basarat.gitbooks.io/typescript/docs/options/strictNullChecks.html) enabled.
     * If you don't use this option, every value can potentially be null, so every decoder
     * has to handle null values at some point, which makes everything safe, but is not very pleasent to work with.
     *
     * **Example:**
     *
     * ```typescript
     * // This assumes strictNullChecks
     * interface Point {
     *     x: number,
     *     y: number,
     *     z?: number
     * }
     *
     * // Error TS2345: Argument of type { ... } is not assignable to parameter of type 'DecoderTable<Point>'.
     * //   Types of property 'z' are incompatible.
     * //     Type 'Decoder<number>' is not assignable to type 'OptionalDecoder<number|undefined>'.
     * //       Property '__you_tried_to_use_a_non_optional_Decoder_on_an_optional_field__' is missing in type 'Decoder<number>'.
     * const pointDecoder = Decoder.obj<Point>({
     *     x: Decoder.float,
     *     y: Decoder.float,
     *     z: Decoder.float
     * });
     *
     * // OK - Decoder.optional returns the right type
     * const pointDecoder = Decoder.obj<Point>({
     *     x: Decoder.float,
     *     y: Decoder.float,
     *     z: Decoder.optional(Decoder.float)
     * });
     * ```
     */
    public static optional<T>(decoder: Decoder<T>, defaultValue?: T): OptionalDecoder<T> {
        return new OptionalDecoder<T>(function Decoder$optional(ctx: Context): Result<T|null> {
            if (typeof ctx.value == 'undefined' || ctx.value === null) {
                return Result.succeed(defaultValue || null);
            } else {
                return decoder._run(ctx);
            }
        });
    }


    /**
     * Lazy makes working with recursive data structures, like nested comments, less painfull.
     *
     * In Typescript, you cannot use a `const` or `let` value before it is assigned.
     * Wrapping the decoder inside a `Decoder.lazy(() => decoder)` will work around that,
     * but Typescript will still complain to you that now the type of `decoder` cannot be inferred,
     * because it is recursively used in its own definition, so we also need an additional type annotation.
     *
     * Please also make sure to never wrap `Decoder.field` or `Decoder.fieldOpt` directly inside of lazy.
     * Because of laziness, the Decoder doesn't know yet wether or not it decodes a field or not,
     * so dispatching to the field decoder will not work correctly.
     * Instead, you can wrap the `fieldDecoder` inside.
     *
     * **Example:**
     *
     * ```typescript
     * interface Comment {
     *     message: string;
     *     responses: Comment[]
     * }
     *
     * const commentDecoder = Decoder.obj<Comment>({
     *     message: Decoder.string,
     *     // Important: Wrap the Decoder.array instead of the Decoder.field for the remapping of the properties to work!
     *     responses: Decoder.field('replies', Decoder.lazy(() => Decoder.array(commentDecoder)))
     * });
     * ```
     */
    public static lazy<T>(decoder: () => Decoder<T>): Decoder<T> {
        return new Decoder<T>(function Decoder$lazy(ctx: Context): Result<T> {
            return decoder()._run(ctx);
        });
    }

    /** */
    public static lazyOpt<T>(decoder: () => OptionalDecoder<T>): OptionalDecoder<T> {
        return new OptionalDecoder<T>(function Decoder$lazyOpt(ctx: Context): Result<T|null> {
            return decoder()._run(ctx);
        });
    }



    /**
     * Decodes a single field of an object using a `fieldDecoder`, or fail if the
     * value is not an object, or the field doesn't exist.
     *
     * Note that this is meaningfully different of a field that has a value of `null`,
     * which the `fieldDecoder` might want to handle seperately.
     *
     * This function usually get's called automatically by {@link Decoder.obj}, but you
     * might find it usefull for decoding a single field type, or for changing the structure
     * in an object decoder.
     */
    public static field<T>(field: string, fieldDecoder: Decoder<T>): Decoder<T> {
        return new Decoder<T>(function Decoder$field(ctx: Context): Result<T> {
            if (typeof ctx.value !== 'object' || ctx.value === null || !(field in ctx.value)) {
                return Result.fail(DecoderError.missingField(ctx, field));
            } else {
                return fieldDecoder._run(ctx.goto(field));
            }
        }, true);
    }


    /**
     * Decode a single optional field of an object using the `fieldDecoder`. In contrast to {@link Decoder.field},
     * this decoder will not immediately fail if the field does not exist, but instead relay a `null` or `defaultValue`
     * to the underlying `fieldDecoder`, if that decoder is setup to work on optional values.
     *
     * To ensure decoders can handle optional values, it is usually best to wrap them inside of {@link Decoder.optional}
     * last.
     *
     * Remember that a _field being optional_ is different from a _value being optional_! This function deals with the
     * former case, which are those cases where the _field is actually missing_. The value of a field that is not missing
     * can still be `null` though, which is handled by {@link Decoder.optional}.
     *
     * **Example:**
     *
     * ```typescript
     * const optionalRevisionDecoder = Decoder.fieldOpt('rev', Decoder.optional(Decoder.int), '')
     * ```
     */
    public static fieldOpt<T>(field: string, fieldDecoder: Decoder<T|null>, defaultValue?: T): OptionalDecoder<T> {
        return new OptionalDecoder<T>(function Decoder$fieldOpt(ctx: Context): Result<T|null> {
            if (typeof ctx.value !== 'object' || ctx.value === null || !(field in ctx.value)) {
                return fieldDecoder instanceof OptionalDecoder
                    ? fieldDecoder._run(ctx.pretend(field, defaultValue || null))
                    : Result.succeed(defaultValue || null);
            } else {
                return fieldDecoder._run(ctx.goto(field));
            }
        }, true);
    }


    /**
     * Decode a single entry of an array using the `indexDecoder`. This is similar to {@link Decoder.field},
     * but for arrays. Since this function checks to see if the input is actually a real array, please use
     * that function instead if your object just happens to have a numeric key.
     *
     * This function usually gets called automatically by {@link Decoder.tuple}, but you may find it usefull
     * for reordering tuple elements.
     *
     * **Example:**
     *
     * ```typescript
     * type Pair = [number, number];
     *
     * const reverseTupleDecoder = Decoder.tuple<Pair>(
     *     Decoder.index(1, Decoder.int),
     *     Decoder.index(0, Decoder.int));
     *
     * reverseTupleDecoder.decodeValue([3, 4]) // OK [4, 3]
     * ```
     */
    public static index<T>(index: number, indexDecoder: Decoder<T>): Decoder<T> {
        return new Decoder<T>(function Decoder$index(ctx: Context): Result<T> {
            const arr = ctx.value;
            if (!(arr instanceof Array)) {
                return Result.fail(DecoderError.badPrimitive(ctx, "an array"));
            }

            const realIndex = index < 0 ? arr.length + index : index;
            if (realIndex < 0 || realIndex >= arr.length) {
                return Result.fail(DecoderError.arraySize(ctx, realIndex));
            }

            return indexDecoder._run(ctx.goto(realIndex));
        }, true);
    }


    /**
     * Make a completely custom Decoder based on a type guard.
     */
    public static guard<T>(guard: (value: any) => value is T, message: string): Decoder<T> {
        return new Decoder<T>(function Decoder$guard(ctx: Context): Result<T> {
            const value = ctx.value;
            return guard(value)
                ? Result.succeed(value)
                : Result.fail(DecoderError.fail(ctx, message));
        });
    }

//#endregion

//#region "Inconsitent Structure / Data transformation"

    /**
     * Ignore the JSON and always return the given value instead.
     * This is useful when used together with `oneOf` or `andThen`.
     */
    public static succeed<T>(value: T): Decoder<T> {
        const result = Result.succeed(value);

        return new Decoder<T>(function Decoder$succeed(_ctx: Context) {
            return result;
        });
    }

    /**
     * `Decoder.succeed` variant for optional fields.
     */
    public static succeedOpt<T>(value?: T): OptionalDecoder<T> {
        const result = Result.succeed(value || null);

        return new OptionalDecoder<T>(function Decoder$succeedOpt(_ctx: Context) {
            return result;
        })
    }

    /**
     * Ignore the JSON and always make the decoder fail with a custom error message.
     * This is useful when used together with `oneOf` or `andThen`.
     */
    public static fail<T>(error: string): Decoder<T> {
        return new Decoder<T>(function Decoder$succeed(ctx: Context) {
            return Result.fail(DecoderError.fail(ctx, error));
        });
    }


    /**
     * Additionally map the decoded value to some other type after successfully decoding it.
     */
    public map<U>(transformer: (value: T) => U): Decoder<U> {
        const self = this;
        return new Decoder<U>(function Decoder$map(ctx: Context): Result<U> {
            return self._run(ctx).map(transformer);
        }, this._isField);
    }


    /**
     * After successfully decoding a value, additionally validate the resulting value using a `predicate`
     * function. The decoder will also fail with `message` if this predicate returns false.
     *
     * **Example:**
     *
     * ```typescript
     * const pairDecoder = Decoder.array(Decoder.int)
     *     .filter("Expected a tuple [x, y] of exactly 2 elements!", value => value.length == 2)
     *
     * pairDecoder.decodeString("[4, 3]") // Ok - [3,4]
     * pairDecoder.decodeString("[4, 3, 2]") // Error
     * ```
     */
    public filter(message: string, predicate: (value: T) => boolean): Decoder<T> {
        return this.andThen(function Decoder$filter(value) {
            return predicate(value)
                ? Decoder.succeed(value)
                : Decoder.fail(message);
        });
    }


    /**
     * Filter out `null` and `undefined` values.
     * This returns a new Decoder that fails instead with the supplied message if it encounters one of those values.
     * The returned Decoder no longer contains those values as possible success values, making this useful
     * in combination with `Decoder.function` if you need to wrap a function returning null on a failure.
     */
    public filterOptional<R>(this: Decoder<R | null | undefined>, message: string): Decoder<R> {
        function isNotNull<T>(value: T | undefined | null): value is T {
            return value !== undefined && value !== null;
        }

        return this.filterGuard(isNotNull, message);
    }


    /**
     * Filter out certain types of values using a type guard function.
     */
    public filterGuard<R>(guard: (value: T | R) => value is R, message: string): Decoder<T & R> {
        return this.andThen(function Decoder$guard(value) {
            return guard(value) ? Decoder.succeed(value) : Decoder.fail(message);
        })
    }


    /**
     * Instead of failing, make a new decoder that instead returns a `defaultValue` if something goes wrong.
     *
     * **Example:**
     *
     * ```typescript
     * const int = Decoder.int;
     * const intOrDefault = int.orDefaultValue(0);
     *
     * int.decodeValue(5)    // ==> 5
     * int.decodeValue(3.14) // ==> DecoderError
     *
     * intOrDefault.decodeValue(5)      // ==> 5
     * intOrDefault.decodeValue(3.14)   // ==> 0
     * ```
     */
    public orDefaultValue(defaultValue: T): Decoder<T> {
        return this.orElse(Decoder.succeed(defaultValue));
    }


    /**
     * Tries an alternative decoder on the same input if this one fails for some reason.
     *
     * If you are building a big `orElse` chain, please note that if something goes wrong,
     * the next decoder in the chain will always be tried, even though the current would "fit better".
     * This is especially bad, because in a orElse chain, only the last decoder's errors will be returned.
     * If you are decoding a [discriminated union](https://basarat.gitbooks.io/typescript/docs/types/discriminated-unions.html)
     * of some sort, consider switching to {@link Decoder.andThen} instead for better error messages and performance.
     *
     * @see Decoder.oneOf
     *
     * **Example:**
     *
     * ```typescript
     * interface Success<T> {
     *     value: T;
     *     success: true;
     * }
     *
     * interface Failure {
     *     error: string;
     *     success: false;
     * }
     *
     * type Result<T> = Success<T> | Failure;
     *
     * const successDecoder = <T> (valueDecoder: Decoder<T>) => Decoder.obj<Success<T>>({
     *     value: valueDecoder,
     *     success: Decoder.tag(true)
     * });
     *
     * const failureDecoder = Decoder.obj<Failure>({
     *     error: Decoder.string,
     *     success: Decoder.tag(false)
     * });
     *
     * const resultDecoder = <T> (valueDecoder: Decoder<T>): Decoder<Result<T>> =>
     *     failureDecoder.orElse(successDecoder(valueDecoder));
     * ```
     */
    public orElse<U>(alternative: Decoder<U>): Decoder<T | U> {
        const self = this;

        //
        const isField = this._isField || alternative._isField;

        return new Decoder<T | U>(function Decoder$orElse(ctx: Context): Result<T | U> {
            const result1 = self._run(ctx);
            const result2 = result1.chainLeft(_errors => alternative._run(ctx));
            return result2;
        }, isField);
    }


    /**
     * Try all decoders in order until one of them succeeds.
     * Note that because the next decoder will be tried if something goes wrong,
     * only the error messages of the last decoder will be returned if all of them fail.
     *
     * If you are dealing with versioned data or discriminated unions, consider switching
     * to {@link Decoder.andThen} instead, to improve error messages and performance.
     *
     * @see Decoder.orElse
     *
     */
    public static oneOf<T>(... decoders: Array<Decoder<T>>): Decoder<T> {
        return decoders.reduce(function Decoder$oneOf(result, decoder) {
            return result.orElse(decoder);
        });
    }


    /**
     * Runs a second Decoder on the same input as `this` one, but only if the first one succeeded.
     * Using this, you can first parse a small part of a larger JSON structure to determine the
     * actual Decoder to use. Possible applications might be versioned data, JSON with dynamic types,
     * or tagged unions.
     *
     * **Example:**
     *
     * ```typescript
     * const configDecoder = Decoder.field('version', Decoder.int).andThen(version => {
     *     switch(version) {
     *         case 3:  return config3Decoder;
     *         case 4:  return config4Decoder;
     *         default: return Decoder.fail("Only Versions 3 and 4 are supported!");
     *     }
     * })
     * ```
     */
    public andThen<U>(continuation: (value: T) => Decoder<U>): Decoder<U> {
        const self = this;
        return new Decoder<U>(function Decoder$andThen(ctx: Context): Result<U> {
            const result1 = self._run(ctx);
            const result2 = result1.chain(value1 => continuation(value1)._run(ctx));
            return result2;
        }, this._isField);
    }


    /**
     * Use an 'Either' type to pull errors into a successful value, making
     * the Decoder no longer fail if something goes wrong, but instead
     * call the `Right` constructor on the value.
     *
     * @param Left 'Either' object constructor for the Failure case
     * @param Right 'Either' object constructor for the Success case
     */
    public fold<U>(Left: (errors: DecoderError[]) => U, Right: (value: T) => U): Decoder<U> {
        const self = this;
        return new Decoder<U>(function Decoder$fold(ctx: Context): Result<U> {
            return Result.succeed(self._run(ctx).match(Right, Left));
        });
    }
//#endregion
}







// Functions in Typescript are bivariant, meaning that a function of type `() => number|string`
// is assignable to a variable of type `() => number`, even though that allows us to assign a string
// value to a number field, breaking the type system.
// They argue that it is easier that way to implement event handlers...
//
// To work around this problem, we create a subclass, which isn't completely broken.
//
// Also note that we cannot move this class into its own file because of circular dependecies
class OptionalDecoder<T> extends Decoder<T | null> {
    // Typescript allows you to assign parent instances to variables of the child type,
    // if the child class is structurally equivalent to the parent.
    // To work around this, we create a private empty variable, making the type actually distinct.
    private __you_tried_to_use_a_non_optional_Decoder_on_an_optional_field__: never;

    public constructor(
        _run: (ctx: Context) => Result<T | null>,
        _isField: boolean = false
    ) {
        super(_run, _isField);
    }


    // Override these functions to make them return OptionaDecoders,
    // making the API more chain-able without losing optional fields type checking on the way.

    map<U>(transformer: (value: T | null) => U): OptionalDecoder<U>;
    map<U>(transformer: (value: T | null) => U): Decoder<U>; // :UnsafeCast this is wrong but enables us to actually change the type signature
    public map<U>(transformer: (value: T | null) => U): OptionalDecoder<U> {
        const self = this;
        return new OptionalDecoder<U>(function OptionalDecoder$map(ctx: Context): Result<U> {
            return self._run(ctx).map(transformer);
        }, this._isField);
    }

    public filter(message: string, predicate: (value: T | null) => boolean): OptionalDecoder<T> {
        const self = this;
        return new OptionalDecoder<T>(function OptionalDecoder$filter(ctx: Context): Result<T | null> {
            return self._run(ctx).chain(result => predicate(result)
                ? Result.succeed(result)
                : Result.fail(DecoderError.fail(ctx, message)));
        }, this._isField);
    }

    public orDefaultValue(defaultValue: T): OptionalDecoder<T> {
        const self = this;
        return new OptionalDecoder<T>(function OptionalDecoder$orDefaultValue(ctx: Context): Result<T | null> {
            return self._run(ctx).chainLeft(_errors => Result.succeed(defaultValue));
        }, this._isField);
    }
}

