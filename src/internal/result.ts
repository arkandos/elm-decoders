import Context from "./context";

import DecoderException from "../decoder-exception";
import DecoderError from "../decoder-error";

export default abstract class Result<T> {
    public static succeed<T>(value: T): Result<T> {
        return new Success(value);
    }

    public static fail<T>(error: DecoderError): Result<T> {
        return new Failure([error]);
    }


    public abstract match<R>(success: (value: T) => R, failure: (errors: DecoderError[]) => R): R;



    public map<U>(transform: (value: T) => U): Result<U> {
        return this.match(
            value => Result.succeed(transform(value)),
            errors => this._unsafeCastError()
        );
    }


    public ap<P, R>(this: Result<((arg: P) => R)>, mValue: Result<P>): Result<R> {
        return this.match(
            fn => mValue.match(
                value => Result.succeed(fn(value)),
                errors => mValue._unsafeCastError()
            ),
            errors => mValue.match(
                value => this._unsafeCastError(),
                errors2 => new Failure(errors.concat(errors2))
            )
        );
    }


    public chain<U>(continuation: (value: T) => Result<U>): Result<U> {
        return this.match(
            value => continuation(value),
            errors => this._unsafeCastError()
        );
    }


    public chainLeft<U>(alternativeContinuation: (errors: DecoderError[]) => Result<U>): Result<T|U> {
        return this.match<Result<T|U>>(
            value => this,
            errors => alternativeContinuation(errors)
        );
    }


    public static sequence<T>(listOfResults: Array<Result<T>>): Result<T[]> {
        const append = (list: T[]) => (item: T) => {
            list.push(item); // :LocalMutability
            return list;
        };

        return listOfResults.reduce((acc, value) => {
            return acc.map(append).ap(value);  // :LocalMutability
        }, Result.succeed<T[]>([]));
    }


    public unwrap(): T {
        return this.match(
            value => value,
            errors => {
                throw new DecoderException(errors)
            });
    }


    private _unsafeCastError<U>(): Result<U> {
        return this.match(
            value => { throw new TypeError("INTERNAL: Tried to cast a Success<T> to a Failure<U>!"); },
            errors => this as any as Result<U> // :TypeDependency We can cast here because Failure does not depend on T
        );
    }
}


class Success<T> extends Result<T> {
    public constructor(
        public readonly value: T
    ) {
        super();
    }

    public match<R>(success: (value: T) => R, failure: (errors: DecoderError[]) => R): R {
        return success(this.value);
    }
}

class Failure<T> extends Result<T> {
    public constructor(
        public readonly errors: DecoderError[]
    ) {
        super();
    }

    public match<R>(success: (value: T) => R, failure: (errors: DecoderError[]) => R): R {
        return failure(this.errors);
    }
}
