
export default class Context {
    private constructor(
        public readonly value: any|null|undefined,
        public readonly path: Array<string|number>
    ) { }

    public static of(value: any): Context {
        return new Context(value, ['$']);
    }

    public goto(prop: string|number) {
        return new Context(this.value[prop], this.path.concat([prop]));
    }

    public pretend(prop: string|number, value: any|null|undefined) {
        return new Context(value, this.path.concat([prop]));
    }
}
