import Decoder from '../../dist/decoder';

interface Comment {
    message: string;
    responses: Comment[]
}

const commentDecoder: Decoder<Comment> =
    Decoder.obj<Comment>({
        message: Decoder.string,
        responses: Decoder.field('replies', Decoder.lazy(() => Decoder.array(commentDecoder)))
    });

console.log(commentDecoder.decodeValue({
    message: "Hello, Sailor!",
    replies: [
        {
            message: "What a wonderful message!",
            replies: []
        },
        {
            message: "Thank you!",
            replies: [
                {
                    message: "Have a nice day!",
                    responses: []
                }
            ]
        }
    ]
}));

