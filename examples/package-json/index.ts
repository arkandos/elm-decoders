import Decoder from '../../dist/decoder';

import fs = require('fs');
import semver = require('semver');


// A simplified version of the full package.json - Type

interface Dependencies {
    [packageName: string]: semver.Range
}

interface PackageJson {
    private?: boolean;
    name: string;
    version: semver.SemVer,
    dependencies: Dependencies
    devDependencies: Dependencies
}

// Parse a string, then pass it to the parseRange function
const rangeDecoder = Decoder.class(semver.Range, Decoder.string, Decoder.succeedOpt(true));

// Parse a string, then pass it to the parseVersion function.
// semver.parse returns `null` if the version is not valid, so
// we use `Decoder.filter` to validate the version afterwards.
const semverDecoder =
    Decoder.function(semver.parse, Decoder.string, Decoder.succeedOpt(true))
        .filterOptional("Version is not valid!");

// Dependencies always contain a Range as their value.
// There are no required fields, so the second parameter be empty.
const dependenciesDecoder = Decoder.dict<Dependencies>(rangeDecoder, {});

// Here we combine everything together to a PackageJson - Interface Decoder.
const packageJsonDecoder = Decoder.obj<PackageJson>({
    private: Decoder.optional(Decoder.boolean),
    name: Decoder.string,
    version: semverDecoder,
    dependencies: dependenciesDecoder,
    devDependencies: dependenciesDecoder
});


const packageJsonStr = fs.readFileSync('./package.json', 'utf-8');
// Try it out on the local package.json!
// `runString` calls the first (left) function on a failure, and the second (right) function on success.
packageJsonDecoder.runString(console.error, console.log, packageJsonStr);
